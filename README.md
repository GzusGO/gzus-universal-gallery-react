# Gzus Universal gallery react 

![demo](demo.png)

Demo of developmen on 24 hours of an from scratch caroucel and filter by name rest items on react. 


## About this repo
I just wanted to have a proof of work for my future teamwork.

So, I did this very quick demo about some of my skills on API planning, node.js, react.
I aspire this project to be a reference of the work I can make. This repo it’s not mean to be a production quality application but instead it is just a demo of what I am able to do within few hours in a week to show up my skills on those technologies I named before.
I work 8 hours for a day and put some extra hours and effort to complete this project. You’ll probably find some typos, but that’s totally OK. Just let me know!
As for me, I’m a man of code, coffee, business and travels. Wake up at 5AM like a champ, have my Starbucks’ caramel macchiato every morning, walk my dog, do some exercise and get ready for a new day of creativity, coding and innovation.
I love to read a lot, play guitar, study new technologies and coding trends and of course, I love my girlfriend.
Not a regular guy, though! Because I put extra effort and ideas into my personal projects and my clients’ so they feel happier every time I deliver my work. Always a step ahead ;)
Thanks for reading this,

Gzus.

## Getting Started with Create React App
set node lts/fermium (-> v14.17.3)

> nvm use lts/fermium 

load dependecies

> npm ci 

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

