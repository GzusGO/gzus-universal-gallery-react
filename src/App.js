import { useState, useEffect } from 'react'
import './App.css'
import Footer from './componnets/Footer'
import Header from './componnets/Header'
import Carousel from './componnets/Carousel'
import PopUp from './componnets/PopUp'

function App() {
// library api: 
// i did't find a api with planets but with products instead, 
// i will put labels so you can see what is the filter selecting.
// this route will return a maximun of 20 items so is safe to fetch all.
const apiUrl="https://fakestoreapi.com/products"

  // creating a state for products 
  const [products, setProducts] = useState([])
  // cretaing state filter values: 
  const [findNameValue, setFindName] = useState('')
  // cretaing state selected Item ID:
  const [SelectedItemId, setItem] = useState('')
  // cretaing state selected Item:
  const [SelectedItem, setSelectedItem] = useState({})
  // cretaing state selected data loaded status:
  const [dataLoaded, setDataLoaded] = useState(false)

  // loading state after fetchin data for the products: 
  useEffect(() => {
    const getProducts = async () => {
      const productsFromServer = await fetchProducts()
      setProducts(productsFromServer)
      setDataLoaded(true)
    }

    getProducts()
  }, [])

  // Fetch Products
  const fetchProducts = async () => {
    const res = await fetch(apiUrl)
    const data = await res.json()
    console.log( "products fetched:", data)
    return data
  }

  // Fetch Product
  const fetchProduct = async (id) => {
      const res = await fetch(`${apiUrl}/${id}`)
      const data = await res.json()

      return data
    
  }

  // setFilter
  const setFilter = (NameValue) => {
    const value =  NameValue ? NameValue : ""
    setFindName(value) 
  }

  // getFilteredProducts
  const getFilteredProducts = ()=> {
      if (findNameValue && findNameValue.length > 0) {
        return products.filter((product) => product.title.toLowerCase().includes(findNameValue.toLowerCase()) )
      } else {
        return products
      }
  }

  const takeALook = async (id) =>{
    console.log("takeALook", id)
    if (id){
      setItem(id)
      const getProduct = await fetchProduct(id);
      setSelectedItem(getProduct)
    } else {
      offTakeALook()
    }
    
  }
  const offTakeALook = () =>{
    setItem('')
    setSelectedItem({})
  }

  // methods for carousel: 

  const getfirstItems = ( numberOfItems, itemsArray )=> {
    // show frist 4  
     const l = itemsArray.length;
     let max = l < numberOfItems ? l : numberOfItems;
     console.log(max);
     return itemsArray.slice(0, max);

  }

  const rotateRight = (itemsArray) => {
     // rotate the array Right
     const l = itemsArray.length; 
     const pivot = itemsArray[l - 1]
     let res = itemsArray.slice(0, l - 1)
     res.unshift(pivot)
     return res 
  };
  const rotateLeft = (itemsArray) => {
      // rotate the array Right
      const l = itemsArray.length; 
      const pivot = itemsArray[0]
      let res = itemsArray.slice(1, l)
      res.push(pivot)
      return res 
  };

  const prevClick = () => {
    // rotate the array right and show fist 4 
    setProducts(rotateRight(products))
    console.log("prevClick", products)
  }

  const nextClick = () => {
      // rotate the array left and show fist 4 
      setProducts(rotateLeft(products))
      console.log("nextClick", products)

  };


  return (
    <div className="App">
      <Header findNameValue={findNameValue}  setFilter={setFilter}  />


      <div className="main">
        <div className="title">
          <div className="container p-2">
            <h1 className="main-title animate__animated animate__zoomIn" >Our Top Products</h1>
          </div>
        </div>
        <div className="container content P-2">

          <Carousel products={getfirstItems(4, getFilteredProducts())} 
                  prevClick={prevClick}
                  nextClick={nextClick}
                  dataLoaded={true}
                  takeALook={takeALook}/>
        </div>

      </div>

      <div >
        { SelectedItemId !== "" && (
          <PopUp product={SelectedItem} offTakeALook={offTakeALook} SelectedItemId={SelectedItemId}/>
      
        ) }
      </div>
      <Footer />
    </div>
  );
}

export default App;
