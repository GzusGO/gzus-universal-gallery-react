import Search from './Search'

const Header = ({findNameValue, setFilter}) => {
  return (
    <div className="Header p-2">
        <header className="container"> 
        <div className="row p-2">
            <div className="col brand-name animate__animated animate__bounceInLeft">
            <i className="bi bi-droplet-half color-2"></i>
            Natural Cosmetics
            </div>

            <div className="col text-end animate__animated animate__bounceInRight">
              <Search findNameValue={findNameValue}  setFilter={setFilter} />
            </div>
        </div>
        </header>

    </div>
  )
}

export default Header