const Footer = () => {
    return (
      <div className="Footer bg-secondary text-light p-2">
          <footer className="container"> 
          <div className="row">
              <div className="col">
              GzusGo.me
              </div>
              <div className="col text-end">
              <p>Copyright &copy; 2021</p>
              </div>
          </div>
          </footer>
  
      </div>
    )
  }

export default Footer