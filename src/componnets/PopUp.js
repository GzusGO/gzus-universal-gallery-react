
const PopUp = ({product, offTakeALook, SelectedItemId}) => {
    return (
        <div className="SelectedItem" onDoubleClick={()=>offTakeALook()}>
            <div className="modal fade" id="exampleModal" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div className="modal-dialog">
                <div className="modal-content">
                <div className="modal-header">
                    <h5 className="modal-title" id="exampleModalLabel">{product.title}</h5>
                    <button type="button" className="btn-close" onClick={()=>offTakeALook()}></button>
                </div>
                <div className="modal-body">
                    <img src={product.image} className="card-img-top" alt="display image" />
                
                </div>
                </div>
            </div>
            </div>
        </div>
    )
  }

export default PopUp