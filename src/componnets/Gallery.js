import Item from './Item'

const Gallery = ({products, takeALook}) => {
    return (
        <div className="row gallery">
            {products.map( (product , index ) => (
              <Item key={index} product={product} takeALook={takeALook}/>
            ))}
        </div>
    )
  }

export default Gallery