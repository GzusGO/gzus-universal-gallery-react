import Gallery from './Gallery'

const Carousel = ({products, takeALook, nextClick, prevClick, dataLoaded}) => {
    return (
      <>
        {dataLoaded ? (
          <div className="carousel__wrap">
            <div className="carousel__inner">
                <button className="carousel__btn carousel__btn--prev" onClick={() => prevClick()}>
                  <i className="bi bi-chevron-left carousel__btn--icon"></i>
                </button>
                <div className="carousel__container">
                  
                  <Gallery products={products} takeALook={takeALook} />

                </div>
                <button className="carousel__btn carousel__btn--next" onClick={() => nextClick()}>
                  <i className="bi bi-chevron-right carousel__btn--icon"></i>
                </button>
            </div>
        </div>

        ) : (
        <div class="spinner-border text-secondary" role="status">
          <span class="visually-hidden">Loading...</span>
        </div>

        )}
        
      </>
    )
  }

export default Carousel