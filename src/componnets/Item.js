const Item = ({product, takeALook}) => {
    return (
        <>
        <div className="col-md-3 col-6 p-2" >
            { product 
                ? (
            <div className="card p-2 animate__animated animate__zoomIn" 
                onClick={()=> takeALook(product.id)}>
                <div className="card-img-canvas">
                    <img src={product.image} 
                    className="card-img-top" 
                    alt="display image" />
                </div>
                <div className="card-body">
                    <h5 className="card-title">{product.title}</h5>
                    <p className="card-description d-none d-md-block">
                        Special for  your skincare routine.
                    </p>
                </div>
            </div>
                )
                : (
                    <div class="spinner-border text-secondary" role="status">
                        <span class="visually-hidden">Loading...</span>
                    </div>
                )
            }
        </div>
        </>
    )
  }

export default Item