const Search = ({findNameValue, setFilter }) => {
    return (
      <div className="search-name-value">
          <label htmlFor="search" className="form-label hidden">Search:</label>
          <input className="form-control search-pruduct" 
                id="search" 
                placeholder="Search any product"
                value={findNameValue}
                onChange={(e) => setFilter(e.target.value)}
                type="text"/>
          <i className="bi bi-search search-icon"></i>
      </div>
    )
  }

export default Search